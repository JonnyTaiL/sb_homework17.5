﻿#include <iostream>
#include <cmath>
using namespace std;

class example
{


private:
    int a = 5;
    int b = 9;
    char letter = 'f';


public:
    void ShowA()
    {
        cout << a << endl;
    }

    void ShowB()
    {
        cout << b << endl;
    }

    void ShowLetter()
    {
        cout << letter << endl;
    }
};

class vector
{
private:
    double x = 6;
    double y = 4;
    double z = 5;
    double result;
    //  sqrt(x* x + y * y + z * z);

public:
    void VectorLength()
    {
        cout << "VectorLength: ";
        cout << sqrt(x * x + y * y + z * z) << endl;
    }
};


int main()
{
    example temp;
    temp.ShowA();
    temp.ShowB();
    temp.ShowLetter();


    vector temp1;
    temp1.VectorLength();

    system("pause");
}

